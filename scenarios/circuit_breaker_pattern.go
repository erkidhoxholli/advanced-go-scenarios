package scenarios

import (
	"fmt"
	"net/http"

	"github.com/sony/gobreaker"
)

var cb *gobreaker.CircuitBreaker

func CircuitBreakerScenario(microserviceUrl string) {
	// setup the settings
	var settings gobreaker.Settings
	settings.Name = fmt.Sprintf("Calling %s", microserviceUrl)
	settings.ReadyToTrip = func(counts gobreaker.Counts) bool {
		failureRatio := float64(counts.TotalFailures) / float64(counts.Requests)
		return counts.Requests >= 3 && failureRatio >= 0.6
	}

	// create circuit breaker with settings
	cb = gobreaker.NewCircuitBreaker(settings)

	// try 5 times and we will notice that after the third consecutive try (with error) the circuit breaker is open and the external service is
	// not called anymore
	for i := 0; i < 5; i++ {
		fmt.Println(fmt.Sprintf("[circuit_breaker_pattern] try number: %d ", i+1))

		cb.Execute(func() (interface{}, error) {
			fmt.Printf("[circuit_breaker_pattern] calling %s \n", microserviceUrl)
			resp, err := http.Get(microserviceUrl)
			if err != nil {
				return nil, err
			}
			return resp, nil
		})
	}

}
