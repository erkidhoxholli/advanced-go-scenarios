package scenarios

import (
	"context"
	"fmt"
	"log"
	"time"
)

type WeatherChanResponse struct {
	value string
	err   error
}

func SlowRequest() {
	ctx := context.WithValue(context.Background(), "maxTimeout", "2s")
	weatherData, err := getWeatherData(ctx)
	if err != nil {
		log.Panicf("[slow_request] Error getting the weather data: %s \n", err.Error())
	}

	fmt.Printf("[slow_request] GetWeatherData data: %s \n", weatherData)
}

func getWeatherData(ctx context.Context) (string, error) {
	maxTimeout := ctx.Value("maxTimeout")
	duration, _ := time.ParseDuration(maxTimeout.(string))
	ctx, cancel := context.WithTimeout(ctx, duration)
	defer cancel()
	respChan := make(chan WeatherChanResponse)

	// execute in a separate goroutine
	go func() {
		weather, err := slowThirdPartyApiCall()
		respChan <- WeatherChanResponse{
			value: weather,
			err:   err,
		}
	}()

	for {
		select {
		case <-ctx.Done():
			return "", fmt.Errorf("[slow_request] Getting the data from slowThirdPartyApiCall took longer than %s.\n", maxTimeout)
		case resp := <-respChan:
			return resp.value, resp.err
		}
	}
}

func slowThirdPartyApiCall() (string, error) {
	timeToResponse, _ := time.ParseDuration("1.2s")
	time.Sleep(timeToResponse)
	return "[slow_request] Sunny day 20 - 25degree", nil
}
