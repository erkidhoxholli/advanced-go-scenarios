package db_swap

type Server struct {
	Store Storer
}

func NewServer(s Storer) *Server {
	return &Server{Store: s}
}

func (s *Server) SwapDB(newStore Storer) {
	s.Store = newStore
}
