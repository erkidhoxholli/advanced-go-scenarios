package db_swap

import "fmt"

type mongoClient struct{}

type MongoStore struct {
	client     *mongoClient
	database   string
	collection string
}

func NewMongoStore() *MongoStore {
	fmt.Printf("[db_swap] new mongodb client connection \n")
	return &MongoStore{
		client:     &mongoClient{},
		database:   "db-test",
		collection: "collection-test",
	}
}

func (store *MongoStore) GetAll() ([]*user, error) {
	fmt.Printf("[db_swap] mongo-store getAll \n")
	return nil, nil
}

func (store *MongoStore) PutOne(*user) error {
	fmt.Printf("[db_swap] mongo-store PutOne \n")
	return nil
}
