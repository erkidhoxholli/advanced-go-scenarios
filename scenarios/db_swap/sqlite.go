package db_swap

import "fmt"

type sqliteClient struct{}

type SqliteStore struct {
	client     *sqliteClient
	database   string
	collection string
}

func NewSqliteStore() *SqliteStore {
	fmt.Printf("[db_swap] new sqlite client connection \n")
	return &SqliteStore{
		client:     &sqliteClient{},
		database:   "db-test",
		collection: "collection-test",
	}
}

func (store *SqliteStore) GetAll() ([]*user, error) {
	fmt.Printf("[db_swap] sqlite-store getAll \n")
	return nil, nil
}

func (store *SqliteStore) PutOne(*user) error {
	fmt.Printf("[db_swap] sqlite-store PutOne \n")
	return nil
}
