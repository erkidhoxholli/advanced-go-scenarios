package db_swap

type user struct {
	id    int
	email string
}

type Storer interface {
	GetAll() ([]*user, error)
	PutOne(*user) error
}
