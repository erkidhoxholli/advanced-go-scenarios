package scenarios

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
)

type user struct {
	Id       int
	Username string
}

type userMap map[int]*user

type server struct {
	db    userMap
	cache userMap
	dbhit int
}

func newServer() *server {
	db := make(map[int]*user)
	for i := 0; i < 100; i++ {
		userId := i + 1
		db[userId] = &user{
			Id:       userId,
			Username: fmt.Sprintf("user_%d", userId),
		}
	}

	return &server{
		db:    db,
		cache: make(map[int]*user),
	}
}

func (s *server) tryCache(id int) (*user, bool) {
	user, found := s.cache[id]
	return user, found
}

func (s *server) handeGetUser(w http.ResponseWriter, r *http.Request) {
	idStr := r.URL.Query().Get("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, "[cache_db_hit] Error parsing the id", http.StatusBadRequest)
	}

	// check if its in cache
	user, found := s.tryCache(id)
	if found {
		json.NewEncoder(w).Encode(user)
		return
	}

	// hit the database
	user, found = s.db[id]
	if !found {
		http.Error(w, fmt.Sprintf("[cache_db_hit] user with id %d not found.", id), http.StatusBadRequest)
	}
	s.dbhit++
	s.cache[id] = user // update the cache
	json.NewEncoder(w).Encode(user)
}
