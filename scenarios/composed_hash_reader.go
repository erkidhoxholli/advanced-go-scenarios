package scenarios

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"io"
)

type HashReader interface {
	io.Reader
	hash() string
}

type hashReader struct {
	*bytes.Reader
	buf *bytes.Buffer
}

func NewHashReader(b []byte) *hashReader {
	return &hashReader{
		Reader: bytes.NewReader(b),
		buf:    bytes.NewBuffer(b),
	}
}

func (h *hashReader) hash() string {
	return hex.EncodeToString(h.buf.Bytes())
}

func HashAndBroadcast(r HashReader) error {
	hash := r.hash()
	fmt.Printf("[composed_hash_reader] Hash: %s. \n", hash)
	return broadcast(r)
}

// Note: we may use HashReader as well, but we dont need access to the hash() in this function
func broadcast(r io.Reader) error {
	b, err := io.ReadAll(r)
	if err != nil {
		return err
	}
	fmt.Printf("[composed_hash_reader] String of bytes: %s. \n", string(b))
	return nil
}
