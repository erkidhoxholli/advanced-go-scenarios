package scenarios

import (
	"fmt"
	"golang.org/x/time/rate"
)

type userPayload struct {
	email    string
	password string
}

func login(user *userPayload) {
	fmt.Printf("[rate_limiter] logging in with email%s and password %s\n", user.email, user.password)
}

func RateLimiterScenario() {
	limiter := rate.NewLimiter(3, 4)
	users := []userPayload{
		{
			email:    "user1@gmail.com",
			password: "qwerty",
		},
		{
			email:    "user1@gmail.com",
			password: "123455",
		},
		{
			email:    "user1@gmail.com",
			password: "111111",
		},
		{
			email:    "user1@gmail.com",
			password: "laptop",
		},
		{
			email:    "user1@gmail.com",
			password: "car",
		},
	}

	for _, user := range users {
		if !limiter.Allow() {
			fmt.Printf("[rate_limiter] stopping login for %+v because limit is reached! \n", user)
			break
		}
		login(&user)
	}

}
