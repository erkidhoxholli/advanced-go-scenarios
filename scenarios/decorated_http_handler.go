package scenarios

import (
	"fmt"
	"net/http"
)

type DB interface {
	Store(string) error
}

type Store struct{}

func (s *Store) Store(value string) error {
	fmt.Printf("[decorate_http_handler] adding a value to db %s: ", value)
	return nil
}

func MakeHttpFunc(db DB, fn httpFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fn(db, w, r)
	}
}

func GetDataHandler(db DB, w http.ResponseWriter, r *http.Request) error {
	return nil
}

func StoreDataHandler(db DB, w http.ResponseWriter, r *http.Request) error {
	return nil
}

type httpFunc func(db DB, w http.ResponseWriter, r *http.Request) error
