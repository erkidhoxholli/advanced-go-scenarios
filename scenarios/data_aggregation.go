package scenarios

import (
	"fmt"
	"sync"
	"time"
)

type UserDataChanResponse struct {
	entity string
	value  int
}

func AggregateUser() {
	start := time.Now()
	respChan := make(chan UserDataChanResponse, 2) // 2 channels to handle nr of messages and nr of likes
	wg := sync.WaitGroup{}

	username := getUser()

	wg.Add(2)
	go getUserNrMessages(username, respChan, &wg)
	go getUserNrLikes(username, respChan, &wg)

	wg.Wait()
	close(respChan)

	for resp := range respChan {
		fmt.Printf("[data_aggregation] Got %v %s. \n", resp.value, resp.entity)
	}

	fmt.Printf("[data_aggregation] Time it took to aggregate the data: %s. \n", time.Since(start))
}

func getUser() string {
	time.Sleep(time.Millisecond * 120)
	return "erkid"
}

func getUserNrMessages(username string, respChan chan UserDataChanResponse, wg *sync.WaitGroup) {
	time.Sleep(time.Millisecond * 150)
	respChan <- UserDataChanResponse{
		entity: "messages",
		value:  4,
	}
	wg.Done()
}

func getUserNrLikes(username string, respChan chan UserDataChanResponse, wg *sync.WaitGroup) {
	time.Sleep(time.Millisecond * 130)
	respChan <- UserDataChanResponse{
		entity: "likes",
		value:  33,
	}
	wg.Done()
}
