package scenarios

import "fmt"

type tempUnit int64

const (
	celcius tempUnit = iota
	fahrenheit
	kelvin
)

type tempResponse struct {
	unit  tempUnit
	value float64
}

func getWaterTemperatureFromThirdParty() tempResponse {
	return tempResponse{
		unit:  fahrenheit,
		value: 10,
	}
}

func getAirTemperatureFromThirdParty() tempResponse {
	return tempResponse{
		unit:  kelvin,
		value: 10,
	}
}

func getWaterTemp() tempResponse {
	resp := getWaterTemperatureFromThirdParty()
	return tempResponse{
		unit:  celcius,
		value: resp.value / 32 * .5556,
	}
}

func getAirTemp() tempResponse {
	resp := getAirTemperatureFromThirdParty()
	return tempResponse{
		unit:  celcius,
		value: resp.value - 273.15, // C = K - 273.15
	}
}

func AntiCorruptionLayerPattern() {
	waterTempThirdParty := getWaterTemperatureFromThirdParty()
	waterTemp := getWaterTemp()
	airTempThirdParty := getAirTemperatureFromThirdParty()
	airTemp := getAirTemp()
	fmt.Printf("[anti_corruption_layer] Water temp %f F -> converted to our domain %f C \n", waterTempThirdParty.value, waterTemp.value)
	fmt.Printf("[anti_corruption_layer] Air temp %f K -> converted to our domain %f C \n", airTempThirdParty.value, airTemp.value)
}
