package scenarios

import (
	"fmt"
)

func NewBuilder(name string) pizzaBuilder {
	return &builder{
		name:      name,
		cheese:    false,
		bacon:     false,
		pepperoni: false,
	}
}

type pizzaBuilder interface {
	Name(value string) pizzaBuilder
	Cheese(value bool) pizzaBuilder
	Bacon(value bool) pizzaBuilder
	Pepperoni(value bool) pizzaBuilder
	Build() string
}

type builder struct {
	name      string
	cheese    bool
	bacon     bool
	pepperoni bool
}

func (b *builder) Name(name string) pizzaBuilder {
	b.name = name
	return b
}

func (b *builder) Cheese(value bool) pizzaBuilder {
	b.cheese = value
	return b
}

func (b *builder) Bacon(value bool) pizzaBuilder {
	b.bacon = value
	return b
}

func (b *builder) Pepperoni(value bool) pizzaBuilder {
	b.pepperoni = value
	return b
}

func (b *builder) Build() string {
	return fmt.Sprintf("[pizza_builder] Preparing %s. \n", b.name)
}
