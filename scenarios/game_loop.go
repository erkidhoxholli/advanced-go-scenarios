package scenarios

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

type Player struct {
	health int
	mu     sync.RWMutex
}

func NewPlayer() *Player {
	return &Player{health: 100}
}

func RenderUI(p *Player) {
	ticker := time.NewTicker(time.Second)
	for {
		p.mu.RLock() // only read
		fmt.Printf("[game_loop] health: %d\r", p.health)
		p.mu.RUnlock()
		<-ticker.C
	}
}

func StartGameLoop(p *Player) {
	ticker := time.NewTicker(time.Millisecond * 250)
	for {
		p.mu.Lock() // read and write
		p.health -= rand.Intn(40)
		if p.health <= 0 {
			fmt.Println("[game_loop] Game over")
			break
		}
		p.mu.Unlock()
		<-ticker.C
	}
}
