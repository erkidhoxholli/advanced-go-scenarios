package main

import (
	"advaced-go/scenarios"
	"advaced-go/scenarios/db_swap"
	"fmt"
	"net/http"
)

func main() {
	/*******************************
		Scenario 1. "Slow request"
		Tags: context, goroutine, channels
		Lets suppose we have to communicate with a third party system or different microservices. We add a limit timeout of 1s.
		If the calls to the third party take more than that we terminate and return an error.
		In our example we have a timeout of 1s. By default the third party takes 1.2s so we terminate.
		Adjust the maxTimeout to 2s for example to see a successful response.
	*******************************/
	scenarios.SlowRequest()

	/*******************************
		Scenario 2. "Data aggregation"
		Tags: context, goroutine, channels, wait groups, deadlock
		Lets suppose we have to make 1 api call to get user data (such as username).
		After we get the user data we need to make 2 calls to get nr of messages and likes that a user has based on that username.
		Lets say getUserApiCall -> 50ms, getUserNrLikes -> 60ms, getUserNrMessages -> 100ms
		Without using gorutines this would take 50 + { 60 + 100 } = 210ms
		For such case we use 2 goroutines and we syncronize them using WaitGroup. In such case it would take:
		50 + 100 = 150ms, because the getUserNrMessages is the slowest call.
	*******************************/
	scenarios.AggregateUser()

	/*******************************
		Scenario 3. "Pizza builder"
		Tags: design patterns, builder pattern
		Using the Builder design pattern to build a pizza the same way it is used to build a Http request
		NewBuilder creates a builder given a pizza name.
		We are going to use this so we don't leak the actual builder and have to worry about null/empty values on the builder itself.
	*******************************/
	pizzaBuilder := scenarios.NewBuilder("Diavola pizza").Bacon(true).Cheese(true).Pepperoni(false).Build()
	fmt.Println(pizzaBuilder)

	/*******************************
		Scenario 4. "Hash reader using composed interfaces"
		Tags: ioreader, composed interfaces
	*******************************/
	payload := []byte("compose-hash-reader")
	scenarios.HashAndBroadcast(scenarios.NewHashReader((payload)))

	/*******************************
		Scenario 5. "Cache and db hit"
		Tags: cache, db hit, optimization, waitgroup, goroutines
		You can run cache_db_hit_test.go to check the improvements
	*******************************/

	/*******************************
		Scenario 6. "DB Swap using interfaces"
		Tags: multiple db, interfaces
		Imagine you would like to use multiple databases or swap them at any point in the future.
		You can use a storer interface and implement all the functions for each db type.
	*******************************/
	mongoStore := db_swap.NewMongoStore()
	server := db_swap.NewServer(mongoStore)
	server.Store.GetAll()

	sqliteStore := db_swap.NewSqliteStore()
	server.SwapDB(sqliteStore)
	server.Store.GetAll()

	/*******************************
		Scenario 7. "Game loop"
		Tags: mutex, race conditions, sync.RWMutex
		You have to run with --race flag
		In the UI loop with only read so we use RLock, RUnlock, whereas in the game loop
		we have to read and write so we use Lock, Unlock
	*******************************/
	player := scenarios.NewPlayer()
	go scenarios.RenderUI(player)
	scenarios.StartGameLoop(player)

	/*******************************
		Scenario 8. "Decorated http handler"
		Tags: decorator-pattern, http handler, api server
		Use the decorator pattern to decorate http handle signature with extra functionalities such
		as accessing the database. We can also decorate/inject a server struct into each handler with lots more functionalities
	*******************************/
	store := &scenarios.Store{}
	http.HandleFunc("/get-data", scenarios.MakeHttpFunc(store, scenarios.StoreDataHandler))
	http.HandleFunc("/store-data", scenarios.MakeHttpFunc(store, scenarios.GetDataHandler))

	/********************************
		Scenario 9. "Circuit breaker"
		Tags: circuit breaker pattern, cloud patterns, reliability, sony/gobreaker package
		It is usually used with sync communication between microservices.
	    If another microservice is down we can decide we allow to try a few times (such as 3), but if it returns errors lets say 3 times,
	    then we stop making calls to this microservice and thus we dont stress the microservice anymore for a particilar period(say 5min)
	    and afterwards calls are allowed to go through again.
	*******************************/
	const microserviceUrl = "microservice:3333" // change to a proper url such as https://google.com to test the other usecase
	scenarios.CircuitBreakerScenario(microserviceUrl)

	/*******************************
		Scenario 10. "Rate limiter/Throttling"
		Tags: rate limiter, throttling, brute force, x/time/rate package
		Lets suppose a bad actor would like to attempt to brute force a password for a user. Without rate limiting he may try
	    to make thousands of calls per minute using different passwords. In this case we can use a rate limiter to limit the
	    max number of requests in a particular time window
	*******************************/
	scenarios.RateLimiterScenario()

	/*******************************
		Scenario 11. "Anti corruption layer"
		Tags: design pattern, bad api, ddd
		Lets suppose we get the data from third parties in fahrenheit or kelvin but in the domain context for our app we use only celcius
		for this reason we build an anti corruption layer to handle everything to match our domain.
	*******************************/
	scenarios.AntiCorruptionLayerPattern()

}
